import com.sun.security.jgss.GSSUtil;

public class Opgave_1 {
    public static void main(String[] args) {



        int x = 5;
        int y = 8;
        int z = 3;


        //Case A
        boolean A = x <= 5 && z != y;

        if (A == true)
            System.out.println("A: PASS");
        else
            System.out.println("A: FAIL");


        //case B
        boolean B = x == 5 || x == y && z == 3;

        if (B == true)
            System.out.println("B: PASS");
        else
            System.out.println("B: FAIL");


        //Case C
        boolean C = x / y > z / x;

        if (C == false)
            System.out.println("C: PASS");
        else
            System.out.println("C: FAIL");


        //Case D
        boolean D = !( x != y - z ) == false;

        if (D == false)
            System.out.println("D: PASS");
        else
            System.out.println("D: FAIL");


        //Case E
        boolean E = 2 * x != x || x == 0;

        if (E == true)
            System.out.println("E: PASS");
        else
            System.out.println("E: FAIL");



        //Case F
        boolean F = ! true || ! false;

        if (F == true)
            System.out.println("F: PASS");
        else
            System.out.println("F: FAIL");





    }


}
